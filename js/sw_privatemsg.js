/**
 * JavaScript API for the Sw Privatemessage module.
 *
 * May only be loaded for authenticated users, with the Sw Privatemessage module enabled.
 */
(function ($, Drupal, drupalSettings, storage) {

    "use strict";

    var currentUserID = parseInt(drupalSettings.user.uid, 10);

    Drupal.sw_privatemsg = {

        /**
         * Marks a node as read, store the last read timestamp in client-side storage.
         *
         * @param Number|String nodeID
         *   A node ID.
         */
        markAsRead: function (threadId) {
            $.ajax({
                url: Drupal.url('messages/view/' + threadId + '/read'),
                type: 'POST',
                dataType: 'json',
                success: function (readIds) {
                    console.log(readIds);
                    // If the data is embedded in the page, don't store on the client side.
                    //if (embeddedLastReadTimestamps && embeddedLastReadTimestamps[nodeID]) {
                    //    return;
                    //}

                    //storage.setItem('Drupal.history.' + currentUserID + '.' + nodeID, timestamp);
                }
            });
        }
    };

    // When the window's "load" event is triggered, mark all enumerated nodes as
    // read. This still allows for Drupal behaviors (which are triggered on the
    // "DOMContentReady" event) to add "new" and "updated" indicators.
    window.addEventListener('load', function() {
        if (drupalSettings.sw_privatemsg && drupalSettings.sw_privatemsg.threadsToMarkAsRead) {
            Object.keys(drupalSettings.sw_privatemsg.threadsToMarkAsRead).forEach(Drupal.sw_privatemsg.markAsRead);
        }
    });

})(jQuery, Drupal, drupalSettings, window.localStorage);
