<?php

/**
 * @file
 * Definition of Drupal\sw_privatemsg\SwThreadStorage.
 */

namespace Drupal\sw_privatemsg;

// use Drupal\Core\Database\Connection;
// use Drupal\Core\Cache\CacheBackendInterface;
// use Drupal\Core\Entity\EntityInterface;
// use Drupal\Core\Entity\EntityManagerInterface;
// use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
// use Drupal\Core\Password\PasswordInterface;
// use Drupal\Core\Session\AccountInterface;
// use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller class for users.
 *
 * This extends the Drupal\Core\Entity\Sql\SqlContentEntityStorage class,
 * adding required special handling for user objects.
 */
class SwThreadStorage extends SqlContentEntityStorage {
  public function load($id) {
//    if ($id == 'new') {
//      return $this->create();
//    }
    return parent::load($id);
  }
}
