<?php

/**
 * @file
 * Contains \Drupal\sw_privatemsg\SwThreadListBuilder
 */

namespace Drupal\sw_privatemsg;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\user\Entity\User;


/**
 * Provides a list controller for sw_privatemsg_thread entity.
 */
class SwThreadListBuilder extends EntityListBuilder {

  /**
   * The entity query factory.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $queryFactory;

  /**
   * Constructs a new UserListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Entity\Query\QueryFactory $query_factory
   *   The entity query factory.
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, QueryFactory $query_factory) {
    parent::__construct($entity_type, $storage);
    $this->queryFactory = $query_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity.manager')->getStorage($entity_type->id()),
      $container->get('entity.query')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = t('Subject');
    $header['participants'] = t('Participants');
    $header['last_message'] = t('Last Message');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\sw_privatemsg\Entity\SwPrivatemsgThread */
    $uri = $entity->urlInfo();
    $row['label']['data'] = array(
      '#type' => 'link',
      '#title' => $entity->label(),
      '#suffix' => ' ' . drupal_render($mark),
      '#url' => $uri,
    );
    $participants = $entity->participants->view(array('label' => 'hidden'));
    $row['participants'] = \render($participants);

    $row['last_message'] = '';
    if ($last_message = $entity->getLastMessage()) {
      $last_message = comment_view($last_message);
      $row['last_message'] = \render($last_message);
    }
    // @todo use hook_entity_operation() and add "mark as read" and "mark as unread" operations.
    return $row + parent::buildRow($entity);
  }

  public function load(User $user = NULL) {
    if (empty($user)) {
      $user = \Drupal::currentUser();
    }
    $entity_query = $this->queryFactory->get('sw_privatemsg_thread');
    $entity_query->condition('participants.target_id', $user->id());
    $entity_query->pager(10);
    $entity_query->sort('updated', 'DESC');
    $ids = $entity_query->execute();
    return $this->storage->loadMultiple($ids);
  }

  public function render(User $user = NULL) {
    $build = array(
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#title' => $this->getTitle(),
      '#rows' => array(),
      '#empty' => $this->t('There are no threads yet.'),
    );

    foreach ($this->load($user) as $entity) {
      if ($row = $this->buildRow($entity)) {
        $build['#rows'][$entity->id()] = $row;
      }
    }
    return $build;
  }

}
