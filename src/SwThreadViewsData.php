<?php

/**
 * @file
 * Contains \Drupal\sw_privatemsg\SwThreadViewsData.
 */

namespace Drupal\sw_privatemsg;


use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;
/**
 * Provides the views data for the node entity type.
 */
class SwThreadViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();
    $data['sw_pm_thread']['table']['base']['help'] = t('Threads contains users correspondence.');
    $data['sw_pm_thread']['table']['base']['access query tag'] = 'sw_privatemsg_thread_access';


    $data['sw_pm_thread']['unread_count'] = array(
      'field' => array(
        'title' => t('Unread count'),
        'help' => t('Number of unread comments in the thread.'),
        'id' => 'sw_privatemsg_unread_count',
      ),
    );
    return $data;
  }
}