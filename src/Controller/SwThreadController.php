<?php
/**
 * @file
 * Contains \Drupal\sw_privatemsg\Controller\SwThreadController.
 */

namespace Drupal\sw_privatemsg\Controller;


use Drupal\Core\Controller\ControllerBase;
use Drupal\sw_privatemsg\SwPrivatemsgThreadInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Component\Utility\Tags;
use Drupal\Component\Utility\Unicode;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\entity_reference\EntityReferenceAutocomplete;
use Drupal\Component\Utility\Xss;

class SwThreadController extends ControllerBase {

  /**
   * Constructs a SwThreadController object.
   *
   * @param \Drupal\entity_reference\EntityReferenceAutocomplete $entity_reference_autocompletion
   *   The autocompletion helper for entity references.
   */
  public function __construct(EntityReferenceAutocomplete $entity_reference_autocompletion) {
    $this->entityReferenceAutocomplete = $entity_reference_autocompletion;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_reference.autocomplete')
    );
  }

  /**
   * @todo csfr?
   * Marks a thread as read by the current user right now.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request of the page.
   * @param \Drupal\sw_privatemsg\SwPrivatemsgThreadInterface $thread
   *   The node whose "last read" timestamp should be updated.
   */
  public function readThread(Request $request, SwPrivatemsgThreadInterface $sw_privatemsg_thread) {
    if ($this->currentUser()->isAnonymous()) {
      throw new AccessDeniedHttpException();
    }

    //@todo use cron processing for more than 10 messages.
    $values = array(
      'comment_type' => 'sw_privatemsg_thread',
      'recipients.target_id' => $this->currentUser()->id(),
      'entity_id' => $sw_privatemsg_thread->id(),
      'recipients.is_new' => 1,
    );
    $unread_messages = entity_load_multiple_by_properties('comment', $values);
    $read_messages = array();
    foreach ($unread_messages as $message) {
      // @todo find out why field_definition|restrictions|entity_type is set to user|node after first save without this line.
      $message->entity_id->getValue();
      foreach ($message->recipients as $recipient) {
        $field_data = $recipient->getValue();
        if (!empty($field_data['target_id']) &&$this->currentUser()->id() == $field_data['target_id'] && $field_data['is_new'] == TRUE) {
          $field_data['is_new'] = 0;
          $recipient->setValue($field_data);
          $read_messages[] = $message->id();
          $message->save();
          break;
        }
      }
    }

    foreach ($sw_privatemsg_thread->participants as $participant) {
      $values = $participant->getValue();
      if ($this->currentUser()->id() == $values['target_id'] && $values['unread_count'] != 0) {
        // @todo fixme.
        $values['unread_count'] = 0;
        $participant->setValue($values);
        $sw_privatemsg_thread->save();
        break;
      }
    }
    // Update the history table, stating that this user viewed this node.
    return new JsonResponse($read_messages);
  }

  /**
   * @todo copied from entity_reference. Review and do a cleanup.
   *
   * Autocomplete the label of an entity.
   *
   * @param Request $request
   *   The request object that contains the typed tags.
   * @param string $type
   *   The widget type (i.e. 'single' or 'tags').
   * @param string $field_name
   *   The name of the entity reference field.
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle_name
   *   The bundle name.
   * @param string $entity_id
   *   (optional) The entity ID the entity reference field is attached to.
   *   Defaults to ''.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Throws access denied when either the field storage or field does not
   *   exists or the user does not have access to edit the field.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The matched labels as json.
   */
  public function handleAutocomplete(Request $request, $type, $field_name, $entity_type, $bundle_name, $entity_id) {
    $definitions = $this->entityManager()->getFieldDefinitions($entity_type, $bundle_name);
    if (!isset($definitions[$field_name])) {
      throw new AccessDeniedHttpException();
    }

    $field_definition = $definitions[$field_name];
    $access_control_handler = $this->entityManager()->getAccessControlHandler($entity_type);
    if (!$access_control_handler->fieldAccess('edit', $field_definition)) {
      throw new AccessDeniedHttpException();
    }

    // Get the typed string, if exists from the URL.
    $items_typed = $request->query->get('q');
    $items_typed = Tags::explode($items_typed);
    $last_item = Unicode::strtolower(array_pop($items_typed));

    $prefix = '';
    // The user entered a comma-separated list of entity labels, so we generate
    // a prefix.
    if ($type == 'tags' && !empty($last_item)) {
      $prefix = count($items_typed) ? Tags::implode($items_typed) . ', ' : '';
    }

    // @todo find a better way to exclude anonymous users here.
    $matches = $this->entityReferenceAutocomplete->getMatches($field_definition, $entity_type, $bundle_name, $entity_id, $prefix, $last_item);
    $anonymous = $this->config('user.settings')->get('anonymous');
    foreach ($matches as $key => $match) {
      if ($match['label'] == $anonymous) {
        unset($matches[$key]);
        break;
      }
    }
    return new JsonResponse(array_values($matches));
  }

  /**
   * Title callback. Returns title for the thread.
   */
  public function getTitle(Request $request, SwPrivatemsgThreadInterface $sw_privatemsg_thread) {
    return Xss::filter($sw_privatemsg_thread->label());
  }
} 