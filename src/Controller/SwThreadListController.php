<?php

/**
 * @file
 * Contains \Drupal\sw_privatemsg\Controller\SwThreadListController.
 */

namespace Drupal\sw_privatemsg\Controller;

use Drupal\Core\Entity\Controller\EntityListController;
use Symfony\Component\HttpFoundation\Request;
use Drupal\user\Entity\User;

/**
 * Defines a controller to list blocks.
 */
class SwThreadListController extends EntityListController {

  /**
   * Shows the block administration page.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return array
   *   A render array as expected by drupal_render().
   */
  public function _listing(User $user = NULL, Request $request = NULL) {
    return $this->entityManager()->getListBuilder('sw_privatemsg_thread')->render($user, $request);
  }

}
