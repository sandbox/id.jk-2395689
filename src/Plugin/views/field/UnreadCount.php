<?php
/**
 * Created by PhpStorm.
 * User: lu4nik
 * Date: 16.12.14
 * Time: 15:40
 */

namespace Drupal\sw_privatemsg\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\field\FieldPluginBase;

/**
 * Field handler to return unread count.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("sw_privatemsg_unread_count")
 */
class UnreadCount extends FieldPluginBase {

  public function query() {

  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $thread = $this->getEntity($values);
    return '<span class="unread-count">' . (int) $thread->getUnreadCount() . '</span>';
  }

}