<?php
/**
 * Created by PhpStorm.
 * User: lu4nik
 * Date: 16.12.14
 * Time: 15:40
 */

namespace Drupal\sw_privatemsg\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\Numeric;

/**
 * Field handler to return unread count.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("sw_privatemsg_current_numeric")
 */
class CurrentNumeric extends Numeric {

  public function query() {
    $this->ensureMyTable();
    // Add the field.
    $params = $this->options['group_type'] != 'group' ? array('function' => $this->options['group_type']) : array();
    $params['function'] = 'buu';
    $this->field_alias = $this->query->addField($this->tableAlias, 'recipients_is_new', NULL, $params);

    $this->addAdditionalFields();
  }
}