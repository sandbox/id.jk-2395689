<?php

/**
 * @file
 * Contains \Drupal\sw_privatemsg\Plugin\Derivative\DynamicMessageActions.
 *
 * @todo isnt used for now, but can be used for making action link configurable.
 */

namespace Drupal\sw_privatemsg\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityManager;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic local tasks for content translation.
 */
class DynamicMessageActions extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The base plugin ID
   *
   * @var string
   */
  protected $basePluginId;

  /**
   * @var EntityManager
   */
  protected $entityManager;

  /**
   * Constructs a new ContentTranslationLocalTasks.
   *
   * @param string $base_plugin_id
   *   The base plugin ID.
   */
  public function __construct($base_plugin_id, EntityManager $entity_manager) {
    $this->basePluginId = $base_plugin_id;
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {

    $entity_type = $this->entityManager->getDefinition('user');
    $this->derivatives['sw_privatemsg.thread_add_user'] = array(
        'entity_type' => 'user',
        'title' => 'Send this user a private message',
        'route_name' => 'sw_privatemsg.thread_add_user',
        'appears_on' => array($entity_type->getLinkTemplate('canonical')),
        'route_parameters' => array(),
      ) + $base_plugin_definition;
    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}