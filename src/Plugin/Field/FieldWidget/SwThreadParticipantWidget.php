<?php

/**
 * @file
 * Contains \Drupal\sw_privatemsg\Plugin\Field\FieldWidget\SwMessageRecepientWidget.
 */

namespace Drupal\sw_privatemsg\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\entity_reference\Plugin\Field\FieldWidget\AutocompleteTagsWidget;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;

/**
 * Plugin implementation of the 'message_recipients' widget.
 *
 * @FieldWidget(
 *   id = "thread_participant",
 *   label = @Translation("Thread participants widget with autocomplete"),
 *   field_types = {
 *     "sw_pm_thread_participant"
 *   },
 *   multiple_values = TRUE
 * )
 */
class SwThreadParticipantWidget extends AutocompleteTagsWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['target_id']['#autocomplete_route_name'] = 'sw_privatemsg.autocomplete';
    return $element;
  }
}
