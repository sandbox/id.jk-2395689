<?php

/**
 * @file
 * Contains \Drupal\sw_privatemsg\Plugin\Field\FieldWidget\SwMessageRecepientWidget.
 */

namespace Drupal\sw_privatemsg\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'message_recipients' widget.
 *
 * @FieldWidget(
 *   id = "message_recipient",
 *   label = @Translation("Message recipients hidden field"),
 *   field_types = {
 *     "sw_pm_recipient"
 *   },
 *   multiple_values = TRUE
 * )
 */
class SwMessageRecipientWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    return $element;
  }

}
