<?php

/**
 * @file
 * Contains \Drupal\sw_privatemsg\Plugin\Field\FieldType\SwPrivatemsgThreadItem.
 */

namespace Drupal\sw_privatemsg\Plugin\Field\FieldType;


use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataReferenceDefinition;
use Drupal\Core\Entity\TypedData\EntityDataDefinition;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;

/**
 * Plugin implementation of the 'sw_privatemsg_thread' field type.
 *
 * @FieldType(
 *   id = "sw_pm_thread_participant",
 *   label = @Translation("Thread participant"),
 *   description = @Translation("This field stores private message thread participants."),
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList",
 *   constraints = {"ValidReference" = {}}
 * )
 */
class SwThreadParticipantItem extends EntityReferenceItem {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'target_id' => array(
          'description' => 'The ID of the thread participant.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'count' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'started' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'last_updated' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'unread_count' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'last_sent' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
      ),
    );
  }


  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['target_id'] = DataDefinition::create('integer')
      ->setLabel(t('User ID'))
      ->setSetting('unsigned', TRUE);

    $properties['entity'] = DataReferenceDefinition::create('entity')
      ->setLabel('User')
      ->setDescription(t('Message recepient'))
      // The entity object is computed out of the entity ID.
      ->setComputed(TRUE)
      ->setReadOnly(FALSE)
      ->setTargetDefinition(EntityDataDefinition::create('user'));

    $properties['count'] = DataDefinition::create('integer')
      ->setLabel(t('Messages Count'))
      ->setDescription(t('Number of messages in a thread.'))
      ->setSetting('unsigned', TRUE);

    $properties['started'] = DataDefinition::create('integer')
      ->setLabel(t('Started'))
      ->setDescription(t('When the first message of the thread was received.'))
      ->setSetting('unsigned', TRUE);

    $properties['last_updated'] = DataDefinition::create('integer')
      ->setLabel(t('Last updated'))
      ->setDescription(t('When the last message of that thread was received.'))
      ->setSetting('unsigned', TRUE);

    $properties['unread_count'] = DataDefinition::create('integer')
      ->setLabel(t('Unread Count'))
      ->setDescription(t('Number of unread messages in a thread.'))
      ->setSetting('unsigned', TRUE);

    $properties['last_sent'] = DataDefinition::create('integer')
      ->setLabel(t('Last sent'))
      ->setDescription(t('Indicates when this recipient sent the last message.'))
      ->setSetting('unsigned', TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('target_id')->getValue();
    return empty($value);
  }

  /**
   * {@inheritdoc}
   * @todo FIXME. Thread edit is broken, should be fixed in widget.
   */
  public function preSave() {
//    if ($this->getEntity()->isNew()) {
    if (!isset($this->unread_count)) {
      $this->count = 0;
      $this->started = REQUEST_TIME;
      $this->last_updated = REQUEST_TIME;
      $this->unread_count = 0;
      $this->last_sent = 0;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return array(
      'handler' => 'default',
    ) + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return array(
      'target_type' => 'user',
      'target_bundle' => 'user',
    ) + parent::defaultStorageSettings();
  }



  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'target_id';
  }
}
