<?php

/**
 * @file
 * Contains \Drupal\sw_privatemsg\Plugin\Field\FieldType\SwMessageRecepientItem.
 */

namespace Drupal\sw_privatemsg\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataReferenceDefinition;
use Drupal\Core\Entity\TypedData\EntityDataDefinition;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;

/**
 * Plugin implementation of the 'sw_pm_recipient' field type.
 *
 * @FieldType(
 *   id = "sw_pm_recipient",
 *   label = @Translation("SW Message Recipient"),
 *   description = @Translation("This field stores private message recipients."),
 *   default_widget = "message_recipient",
 *   default_formatter = "entity_reference_label",
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList",
 *   constraints = {"ValidReference" = {}}
 * )
 */
class SwMessageRecipientItem extends EntityReferenceItem {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'target_id' => array(
          'description' => 'The ID of the message recipient.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'is_new' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return array(
      'handler' => 'default',
    ) + parent::defaultFieldSettings();
  }

    /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return array(
      'target_type' => 'user',
      'target_bundle' => 'user',
    ) + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['target_id'] = DataDefinition::create('integer')
        ->setLabel(t('User ID'))
        ->setSetting('unsigned', TRUE);

    $properties['entity'] = DataReferenceDefinition::create('entity')
      ->setLabel('User')
      ->setDescription(t('Message recepient'))
      // The entity object is computed out of the entity ID.
      ->setComputed(TRUE)
      ->setReadOnly(FALSE)
      ->setTargetDefinition(EntityDataDefinition::create('user'));

    $properties['is_new'] = DataDefinition::create('integer')
        ->setLabel(t('Is new'))
        ->setSetting('unsigned', TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'target_id';
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();
    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  // public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
  //   $values['value'] = rand(pow(10, 8), pow(10, 9)-1);
  //   return $values;
  // }
}
