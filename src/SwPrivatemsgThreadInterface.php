<?php

/**
 * @file
 * Contains \Drupal\sw_privatemsg\SwPrivatemsgThreadInterface.
 */

namespace Drupal\sw_privatemsg;

use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\UserInterface;

/**
 * Provides an interface defining a SwPrivatemsgThread entity.
 * @ingroup sw_privatemsg
 */
interface SwPrivatemsgThreadInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface{

}
