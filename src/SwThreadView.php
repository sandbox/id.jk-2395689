<?php

/**
 * @file
 * Contains Drupal\sw_privatemsg\Entity SwThreadView
 */

namespace Drupal\sw_privatemsg;


use Drupal\Core\Entity\EntityViewBuilder;

class SwThreadView extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildComponents(array &$build, array $threads, array $displays, $view_mode, $langcode = NULL) {
//    $ids = array();
//    foreach ($threads as $thread) {
//      $ids[] = $thread->id();
//    }
//    $build['#attached']['js'][] = array(
//      'data' => 'window.addEventListener("load",function(){Drupal.sw_privatemsg.markAsRead([' . implode(',', $ids) . ']);},false);',
//      'type' => 'inline',
//    );
//    $build[0]['#attached']['library'][] = 'sw_privatemsg/sw_privatemsg';
    parent::buildComponents($build, $threads, $displays, $view_mode, $langcode);

  }

} 