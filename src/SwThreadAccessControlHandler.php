<?php

/**
 * @file
 * Contains \Drupal\sw_privatemsg\SwThreadAccessControlHandler.
 */

namespace Drupal\sw_privatemsg;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;

class SwThreadAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, $langcode, AccountInterface $account) {
    /** @var \Drupal\Core\Entity\EntityInterface|\Drupal\user\EntityOwnerInterface $entity */

    // if ($account->hasPermission('administer sw_privatemsg settings')) {
    //   return AccessResult::allowed();
    // }
    switch ($operation) {
      case 'view':
        if ($account->hasPermission('administer sw_privatemesg')) {
          return AccessResult::allowed()->cachePerRole();
        }
        return AccessResult::allowedIf($entity->isThreadParticipant($account))->cachePeruser()->cacheUntilEntityChanges($entity);

      // @todo who should be able to edit threads?
      case 'update':
        if ($account->hasPermission('administer sw_privatemesg')) {
          return AccessResult::allowed()->cachePerRole();
        }
        return AccessResult::allowedIf($entity->isThreadParticipant($account) && $entity->getOwnerId() == $account->id())->cachePeruser()->cacheUntilEntityChanges($entity);

      case 'delete':
        if ($account->hasPermission('administer sw_privatemesg')) {
          return AccessResult::allowed()->cachePerRole();
        }
        return AccessResult::allowedIf($entity->isThreadParticipant($account))->cachePeruser()->cacheUntilEntityChanges($entity);

      default:
        // No opinion.
        return AccessResult::neutral()->cachePerRole();
    }
  }
}
