<?php
/**
 * @file
 * Contains Drupal\sw_privatemsg\Form\SwThreadForm.
 */
namespace Drupal\sw_privatemsg\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Entity\EntityFormBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Render\Element;
use Drupal\comment\CommentInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;


/**
 * Form controller for the sw_privatemsg entity edit forms.
 *
 * @ingroup sw_privatemsg
 */
class SwThreadForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager'),
      $container->get('entity.form_builder'),
      $container->get('module_handler')
    );
  }


  /**
   * Constructs a new SwThreadForm.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager service.
   * @param \Drupal\Core\Session\EntityFormBuilder $current_user
   *   The current user.
   */
  public function __construct(EntityManagerInterface $entity_manager, EntityFormBuilder $entity_form_builder, ModuleHandlerInterface $module_handler) {
    parent::__construct($entity_manager);
    $this->entityFormBuilder = $entity_form_builder;
    // Can't use parameter upcasting for _entity_form routes for some reasons. This allows to use this class both as _form and Entity_form route.
    if (!$this->getEntity()) {
      $this->setEntity($this->entityManager->getStorage('sw_privatemsg_thread')->create(array()));
    }
    $this->setModuleHandler($module_handler);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, AccountInterface $user = NULL) {
    $form = parent::buildForm($form, $form_state);
    if (!empty($user)) {
      $form['participants'] = array(
        '#type' => 'value',
        '#value' => array(
          array('target_id' => $user->id()),
        )
      );
    }

    if ($this->getEntity()->isNew()) {
      // @todo figure out how to fetch fields from comment entity.
      // We can't call $this->entityFormBuilder->getForm($comment) here for this cause it will break some internal logic.
      $form['comment_body'] = array(
        '#type' => 'text_format',
        '#title' => $this->t('Message'),
        '#required' => TRUE,
        '#weight' => 10,
      );

      $form['actions']['submit']['#value'] = $this->t('Send message');
    }
    else {
      // @todo Participants field breaks on the edit. Fix widget.
//      hide($form['participants']);
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $entity->save();
    $form_state->setRedirect('sw_privatemsg.thread_view', array('sw_privatemsg_thread' => $entity->id()));
     $comment = $this->entityManager->getStorage('comment')->create(array(
       'subject' => $form_state->getValue('subject'),
       'comment_body' => $form_state->getValue('comment_body'),
       'entity_id' => $entity->id(),
       'entity_type' => 'sw_privatemsg_thread',
       'field_name' => 'sw_pm_message',
       'status' => CommentInterface::PUBLISHED,
       'uid' => \Drupal::currentUser()->id(),
     ));
     $comment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity() {
    return $this->entity;
  }
}
