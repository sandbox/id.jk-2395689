<?php

/**
 * @file
 * Defines Drupal\sw_privatemsg\Form\SwPrivatemsgSettingsForm.
 */
namespace Drupal\sw_privatemsg\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class SwPrivatemsgSettingsForm extends ConfigFormBase {

  /**
   * @{@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * @{@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['todo']['#markup'] = '@Todo - there shold be privatemessage settings';
    return parent::buildForm($form, $form_state);
  }

  /**
   * @{@inheritdoc}
   */
  public function getFormId() {
    return 'privatemsg_admin_settings';
  }
}

