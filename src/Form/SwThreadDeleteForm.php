<?php
/**
 * Created by PhpStorm.
 * User: lu4nik
 * Date: 24.12.14
 * Time: 16:28
 */

namespace Drupal\sw_privatemsg\Form;


use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class SwThreadDeleteForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the thread %title?', array('%title' => $this->entity->subject->value));
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    // Point to the entity of which this comment is a reply.
    return $this->entity->urlInfo();
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($this->entity->participants as $key => $participant) {
      if ($participant->getValue()['target_id'] == \Drupal::currentUser()->id()) {
        unset($this->entity->participants[$key]);
        $this->entity->save();
      }
    }
    drupal_set_message($this->t('Thread has been deleted.'));

    $form_state->setRedirectUrl(new Url('sw_privatemsg.thread_list'));
  }

}