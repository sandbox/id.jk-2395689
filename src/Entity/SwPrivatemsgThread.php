<?php
/**
 * @file
 * Contains \Drupal\sw_privatemsg\Entity\SwPrivatemsgThread.
 */
namespace Drupal\sw_privatemsg\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\sw_privatemsg\SwPrivatemsgThreadInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Session\AccountInterface;

//Drupal\views\EntityViewsData
/**
 * @todo add methods to the interface.
 * Defines the SwPrivatemsgThread entity.
 *
 * @ingroup sw_privatemsg
 *
 * @ContentEntityType(
 *   id = "sw_privatemsg_thread",
 *   label = @Translation("Privatemessage Thread"),
 *   handlers = {
 *     "storage"      = "Drupal\sw_privatemsg\SwThreadStorage",
 *     "view_builder" = "Drupal\sw_privatemsg\SwThreadView",
 *     "list_builder" = "Drupal\sw_privatemsg\SwThreadListBuilder",
 *     "form" = {
 *       "default" = "Drupal\sw_privatemsg\Form\SwThreadForm",
 *       "delete" = "Drupal\sw_privatemsg\Form\SwThreadDeleteForm"
 *     },
 *     "access" = "Drupal\sw_privatemsg\SwThreadAccessControlHandler",
 *     "views_data" = "Drupal\sw_privatemsg\SwThreadViewsData",
 *   },
 *   base_table = "sw_pm_thread",
 *   admin_permission = "administer sw_privatemsg entity",
 *   fieldable = TRUE,
 *  translatable = FALSE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "subject",
 *     "uuid" = "uuid",
 *   },
 *   field_ui_base_route = "sw_privatemsg.admin_settings",
 *   links = {
 *     "canonical" = "sw_privatemsg.thread_view",
 *     "delete-form" = "sw_privatemsg.thread_delete",
 *     "edit-form" = "sw_privatemsg.thread_edit",
 *   }
 * )
 */
class SwPrivatemsgThread extends ContentEntityBase implements SwPrivatemsgThreadInterface {

  /**
   * {@inheritdoc}
   */
  public function id() {
    $id = parent::id();
//    if (empty($id)) {
//      return 'new';
//    }
    return $id;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Thread entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Contact entity.'))
      ->setReadOnly(TRUE);

    $fields['subject'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Subject'))
      ->setDescription(t('Thread subject.'))
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -5,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -5,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of the thread author.'))
      ->setRevisionable(FALSE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\sw_privatemsg\Entity\SwPrivatemsgThread::getCurrentUserId')
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ))->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created on'))
      ->setDescription(t('The time that the thread was created.'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'timestamp',
        'weight' => 0,
      ))->setDisplayOptions('form', array(
        'type' => 'datetime_timestamp',
        'weight' => 10,
      ))->setDisplayConfigurable('form', TRUE);

    $fields['updated'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Updated'))
      ->setDescription(t('The time that the thread was last updated.'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE);

    return $fields;
  }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId() {
    return array(\Drupal::currentUser()->id());
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('updated')->value;
  }

  /**
   * Returns last comment in the thread.
   */
  public function getLastMessage() {
    return entity_load('comment', $this->sw_pm_message->first()->cid);
  }

  /**
   * Determines if thread contains unread messages for current user.
   */
  public function hasUnreadMessages() {
    $count = $this->getUnreadCount();
    return is_int($count) && $count > 0;
  }

  /**
   * Get number of unread messages in the thread..
   */
  public function getUnreadCount() {
    $data = $this->getParticipantData(\Drupal::currentUser());
    return !empty($data->unread_count) ? $data->unread_count : NULL;
  }

  /**
   * Determines participant field for given user.
   */
  public function getParticipantData(AccountInterface $account) {
    foreach ($this->participants as $participant) {
      if ($participant->target_id == $account->id()) {
        return $participant;
      }
    }
  }

  /**
   * Determines if given user is thread participant.
   */
  public function isThreadParticipant(AccountInterface $account) {
    $data = $this->getParticipantData($account);
    return !empty($data);
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    if ($this->isNew() && !$this->isThreadParticipant(\Drupal::currentUser())) {
      $participants = $this->participants->getValue();
      $participants[] = array(
        'target_id' => \Drupal::currentUser()->id(),
      );
      $this->participants->setValue($participants);
    }
//    if (!$this->subject->getValue()) {
//      $this->subject->setValue($this->t('No subject'));
//    }
  }
}
