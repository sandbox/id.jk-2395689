<?php

/**
 * @file
 * Provide views data for sw_privatemsg.module.
 */

use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_field_views_data().
 */
function sw_privatemsg_field_views_data(FieldStorageConfigInterface $field_storage) {
//  foreach ($data as $table_name => $table_data) {
//    $data[$table_name][$field_storage->getName()]['relationship'] = array(
//      'id' => 'standard',
//      'base' => $target_base_table,
//      'entity type' => $target_entity_type_id,
//      'base field' => $target_entity_type->getKey('id'),
//      'relationship field' => $field_storage->getName() . '_target_id',
//      'title' => t('@label referenced from @field_name', $args),
//      'label' => t('@field_name: @label', $args),
//    );
//  }


  // In some cases, for example during field deletion "entity_reference" module can be not loaded
  \Drupal::ModuleHandler()->loadInclude('entity_reference', 'inc', 'entity_reference.views');
  $data = entity_reference_field_views_data($field_storage);


//  if ($field_storage->getName() == 'recipients') {
//    foreach ($data as $table_name => $table_data) {
//      if (!empty($table_data[$field_storage->getName() . '_is_new'])) {
//        $data[$table_name][$field_storage->getName() . '_is_new_current']['field'] = array(
//          'id' => 'sw_privatemsg_current_numeric',
//          'title' => t('New message indicator for the current user'),
//          'help' => t('Provides mark if message is new for the current user'),
//        );
//      }
//    }
//  }
  return $data;
}